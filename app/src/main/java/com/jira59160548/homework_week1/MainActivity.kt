package com.jira59160548.homework_week1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    lateinit var diceImage : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollBtn = findViewById<Button>(R.id.rollBtn)
        rollBtn.setOnClickListener { rollDice() }

        val resetBtn = findViewById<Button>(R.id.resetBtn)
        resetBtn.setOnClickListener{ resetDice() }

        diceImage = findViewById(R.id.diceImg)
    }

    private fun rollDice() {
        val randomInt = Random.nextInt(6) + 1
        val drawableResource = when (randomInt) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        diceImage.setImageResource(drawableResource)
        Toast.makeText(this, "The dice value is ${randomInt}", Toast.LENGTH_SHORT).show()
    }

    private fun resetDice() {
        diceImage.setImageResource(R.drawable.empty_dice)
    }
}
